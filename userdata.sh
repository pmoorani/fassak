#!/usr/bin/env bash

sudo apt-get update
sudo apt-get -y install python3-pip python3-dev nginx git
sudo apt-get update
sudo pip3 install virtualenv
cd /home/ubuntu/
git clone https://gitlab.com/pmoorani/fassak.git
chmod -R 777 /home/ubuntu/fassak/
cd fassak/
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py collectstatic --noinput
sudo cp config/gunicorn.service /etc/systemd/system/gunicorn.service
sudo systemctl daemon-reload
sudo systemctl start gunicorn
sudo systemctl enable gunicorn
sudo cp config/fassak /etc/nginx/sites-available/fassak
sudo ln -s /etc/nginx/sites-available/fassak /etc/nginx/sites-enabled
sudo rm /etc/nginx/sites-enabled/default
sudo service nginx restart
sudo systemctl daemon-reload
sudo systemctl restart gunicorn
sudo service nginx restart