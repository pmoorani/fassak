import json
from django.contrib.auth.models import User, Group
from django.http import HttpResponse
from photos.models import Snap
from rest_framework import viewsets, status
from rest_framework.response import Response
from photos.serializers import UserSerializer, GroupSerializer, SnapSerializer
from photos.overrides import AllowPOSTRequest

# Create your views here.
def index(request):
    return HttpResponse("Hello, World")

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = [AllowPOSTRequest,]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=ValueError):
            created = serializer.create(validated_data=request.data)
            if created:
                return Response({'msg': 'User registered successfully!',
                                 'success': 1}, status=status.HTTP_201_CREATED)
            else:
                return Response({'msg': 'User not registered!',
                                 'success': 0}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.error_messages, status=status.HTTP_400_BAD_REQUEST)


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class SnapViewSet(viewsets.ModelViewSet):
    queryset = Snap.objects.all()
    serializer_class = SnapSerializer

    def create(self, request, *args, **kwargs):
        serializer = SnapSerializer(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=ValueError):
            created = serializer.create(validated_data=request.data)
            if created:
                serializerr = SnapSerializer(created)
                return Response({'data': serializerr.data,
                                    'msg': 'Image successfully uploaded!',
                                    'success': 1}, status=status.HTTP_201_CREATED)
            else:
                return Response({'msg': 'Image could not be uploaded!',
                                        'success': 0}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.error_messages, status=status.HTTP_400_BAD_REQUEST)


def jwt_response_payload_handler(token, user=None, request=None):
    """
    Token generation middleware.
    """
    return {
        'token': token,
        'user_id': UserSerializer(user, context={'request': request}).data.get('id'),
        'username': UserSerializer(user, context={'request': request}).data.get('username'),
        'first_name': UserSerializer(user, context={'request': request}).data.get('first_name'),
        'last_name': UserSerializer(user, context={'request': request}).data.get('last_name'),
        'role': UserSerializer(user, context={'request': request}).data.get('role'),
        'image': UserSerializer(user, context={'request': request}).data.get('image'),
    }

