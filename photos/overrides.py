from rest_framework import permissions, pagination

## Custom Permissions ##
class AllowPOSTRequest(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            if view.action == 'create':
                return True
            else:
                return False
        elif request.user.is_authenticated:
            if view.action in ['list', 'retrieve']:
                return True
            else:
                return False
        else:
            return True
