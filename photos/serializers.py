from django.contrib.auth.models import User, Group
from photos.models import Snap
from rest_framework import serializers

class SnapSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(slug_field='username', read_only=True)

    class Meta:
        model = Snap
        fields = ('id', 'image', 'user', 'user_id', 'date_created', 'date_modified')
    
    def create(self, validated_data):
        snap = Snap()
        snap.image = validated_data.get('image', '')
        snap.user_id = validated_data.get('user', '')
        snap.save()

        return snap


class UserSerializer(serializers.HyperlinkedModelSerializer):
    snaps = SnapSerializer(many=True, read_only=True)
    mobile = serializers.SerializerMethodField()
    role = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('url', 'id', 'first_name', 'last_name', 'username', 'email',
                  'mobile', 'role', 'image', 'location', 'date_joined', 'snaps')

    def create(self, validated_data):
        print(validated_data)
        user = User.objects.create_user(validated_data['username'],
                                        validated_data['email'],
                                        validated_data['password'], )
        user.first_name = validated_data['first_name']
        user.last_name = validated_data['last_name']
        user.profile.mobile = validated_data.get('mobile', '')
        user.profile.role = int(validated_data.get('role', 3))
        user.profile.image = validated_data.get('image', '')
        user.profile.bio = validated_data.get('bio', '')
        user.profile.location = validated_data.get('location', '')
        user.profile.gender = validated_data.get('gender', '')
        user.save()

        return user

    def get_mobile(self, obj):
        return obj.profile.mobile

    def get_role(self, obj):
        return obj.profile.role

    def get_image(self, obj):
        if obj.profile.image and hasattr(obj.profile.image, 'url'):
            return obj.profile.image.url
        else:
            return None

    def get_location(self, obj):
        return obj.profile.location


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

