from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    bio = models.CharField(max_length=200, blank=True)
    mobile = models.CharField(null=True, max_length=20, name='mobile')
    location = models.CharField(max_length=30, blank=True)
    gender = models.CharField(max_length=6)
    role = models.IntegerField(default=3)
    image = models.ImageField(upload_to='profile_image', blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class Snap(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='snaps')
    image = models.ImageField(upload_to='snaps', blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


post_save.connect(Profile.create_user_profile, sender=User)
post_save.connect(Profile.save_user_profile, sender=User)
