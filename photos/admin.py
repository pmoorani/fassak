from django.contrib import admin
from photos.models import Snap, Profile

# Register your models here.
admin.site.register(Snap)
admin.site.register(Profile)
